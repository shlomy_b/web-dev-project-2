//Shlomo Bensimhon 1837702

/**
 * Fetches a quote from https://ron-swanson-quotes.herokuapp.com/v2/quotes
 * and displays the quote on the DOM. The word is displayed using the RSVP
 * method of reading.
 *
 * @author: shlomy_b
 */


"use Strict"

document.addEventListener("DOMContentLoaded", setup);
let running;
let currentWord;
let global;


 /**
   * The setup function initializes global variables, creates 
   * listeners for the stop/start button, and adds text content 
   * to the button html tag.
   *
   */
function setup(){
    currentWord = 0;
    running = false;
    global = {};
    document.querySelector("#startStop").textContent = "Start";

    if(localStorage.getItem("currentSpeed") == null){
        document.querySelector('#speedChange').value = 100;
        localStorage.setItem("currentSpeed", document.querySelector('#speedChange').value);
    }else{
        document.querySelector('#speedChange').value = localStorage.getItem("currentSpeed");
    }
    document.querySelector("#startStop").addEventListener("click", starter);
    document.querySelector("#speedChange").addEventListener("change", speedChanger);
}

/**
   * The starter function checks to see if the program is running. 
   * If it is, it changes the button text to stop and fetches a 
   * new quote using the getNextQuote method. If its not running,
   * it clears the setInterval and sets the button text to start.
   *
   */
function starter(){
    currentWord = 0;
    if(!running){
        running = true;
        getNextQuote();
        document.querySelector("#startStop").textContent = "Stop"
        //the program is supposed to start here
    }else{
        document.querySelector("#startStop").textContent = "Start";
        running = false;
        clearInterval(global.start);
        //the program is supposed to stop here
        //clear the interval 
    }
}

/**
   * The speedChanger function updates the localStorage variable
   * holding the last selected speed that the user chose.
   *
   */
function speedChanger(){
    localStorage.setItem("currentSpeed", document.querySelector('#speedChange').value);
    // update  window.localStorage item
}

/**
   * The getNextQuote function fetches a new quote from the 
   * https://ron-swanson-quotes.herokuapp.com/v2/quotes API, 
   * and then calls stringSplitter method with the result of the 
   * fetch, which is sent to the displayQuote method. The getNextQuote
   * method also catches any error that might occur with the fetch.
   *
   */
function getNextQuote(){
    fetch('https://ron-swanson-quotes.herokuapp.com/v2/quotes')
    .then (response => response.json())
    .then (json => displayQuote(stringSplitter(json)))
    .catch (error => console.log('There was a problem' + error));
}

/**
   * The stringSplitter function takes a json as input calls toString on it,
   * and finally splits it up using the split method, splitting by a single space.
   * It then returns that array
   *
   * @param {json} json - The json containing the string to be split.
   * @return {array} The array of the split json.
 */
   function stringSplitter(json){
    let array = json.toString().split(' ');
    console.log(array);
    return array;
}

/**
   * The displayQuote function takes an array as input and
   * starts a setInterval, which sends the array over and over.
   * 
 */
function displayQuote(array){
    let wordsPerMin = 60000 / (document.querySelector("#speedChange").value);
    global.start = setInterval(display, wordsPerMin, array);
}

/**
   * The display function takes an array as input and sends 
   * that array to the displayPrint method depending on the
   * length of the currentWord. If the word is longer then 
   * the array, a new quote is fetched and the interval is reset. 
   *
   * @return {array} The array of the split json.
 */
function display(array){
    if(currentWord < array.length){
        displayPrint(array);
        currentWord++;
    }else{
        currentWord = 0;
        clearInterval(global.start);
        if(running){
            getNextQuote();
        }
    }

/**
   * The displayPrint function is a helper function that takes 
   * an array as input and prints every element in that array
   * to the DOM, classifying each letter of each word accordingly 
   * in order to "center" the word.
   *
   * @return {array} The array of the split json.
 */
function displayPrint(array){
    let before = document.querySelector("#before");
    let focus = document.querySelector("#focus");
    let after = document.querySelector("#after");

    if(array[currentWord].length == 1){
        before.textContent  = "    ";
        focus.textContent  = array[currentWord].substring(0, 1);
        after.textContent  = array[currentWord].substring(1);

    }else if(array[currentWord].length > 1 && array[currentWord].length <= 5){
        before.textContent  = "   " + array[currentWord].substring(0, 1);
        focus.textContent  = array[currentWord].substring(1, 2);
        after.textContent  = array[currentWord].substring(2);

    }else if(array[currentWord].length > 5 && array[currentWord].length <= 9){
        before.textContent  = "  " + array[currentWord].substring(0, 2);
        focus.textContent  = array[currentWord].substring(2, 3);
        after.textContent  = array[currentWord].substring(3);

    }else if(array[currentWord].length > 9 && array[currentWord].length <= 13){
        before.textContent  = " " + array[currentWord].substring(0, 3);
        focus.textContent  = array[currentWord].substring(3, 4);
        after.textContent  = array[currentWord].substring(4);

    }else if(array[currentWord].length > 13 ){
        before.textContent  = array[currentWord].substring(0, 4);;
        focus.textContent  = array[currentWord].substring(4, 5);
        after.textContent  = array[currentWord].substring(5);
    }
}
    
}